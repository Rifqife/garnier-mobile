(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
(function($) {
	jQuery(document).ready(function(){
		$('.arrow.left').click(function(){
			if ($('.follow-us-1').hasClass('active-module'))
			{
				$('.follow-us-3').addClass('active-module');
				$('.follow-us-1').removeClass('active-module');
			}
			else if ($('.follow-us-2').hasClass('active-module'))
			{
				$('.follow-us-1').addClass('active-module');
				$('.follow-us-2').removeClass('active-module');
			}
			else if ($('.follow-us-3').hasClass('active-module'))
			{
				$('.follow-us-2').addClass('active-module');
				$('.follow-us-3').removeClass('active-module');
			}
		});
	});
})(jQuery)